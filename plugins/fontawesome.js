import Vue from 'vue'
import { library, config } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
  faFacebookF,
  faTwitter,
  faGooglePlusG,
  faPinterest,
  faInstagram,
  faStumbleupon
} from '@fortawesome/free-brands-svg-icons'
import { faBars, faRss, faCloudDownloadAlt, faBolt } from '@fortawesome/free-solid-svg-icons'
import { faKeyboard, faLightbulb } from '@fortawesome/free-regular-svg-icons'

config.autoAddCss = false
library.add(faBars, faFacebookF, faTwitter, faGooglePlusG, faPinterest, faInstagram, faRss, faStumbleupon, faCloudDownloadAlt, faBolt, faKeyboard, faLightbulb)

Vue.component('font-awesome-icon', FontAwesomeIcon)
